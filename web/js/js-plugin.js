(function ($) {
    let app = {
        self: null,
        options: {},
        init: function (options) {
            console.log("im your plugin");
        },
        test: function (){}
    };

    $.fn.myPlugin = function (o) {
        if (!this || !this.length) return;
        app.self = this;
        if (app[o]) {
            return app[o].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if ($.type(o) === 'object' || !o) {
            return app.init.apply(this, arguments);
        } else {
            $.error('method ' + o + ' does not exist');
        }
    };
})(jQuery.noConflict());